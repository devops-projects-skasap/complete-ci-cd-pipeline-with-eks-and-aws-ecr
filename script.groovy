def testApp() {
    sh 'mvn test'
}

def buildJar() {
    sh 'mvn clean package -Dmaven.test.skip=true'
}

def buildImageDockerHub() {
    withCredentials([usernamePassword(credentialsId: 'DockerHubLogin', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh "docker build -t ${IMAGE_NAME}${TAG} ."
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh "docker push ${IMAGE_NAME}${TAG}"
    }
}

def buildImageECR() {
    withCredentials([usernamePassword(credentialsId: 'ECRLogin', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh "docker build -t ${IMAGE_NAME}${TAG} ."
        sh "echo $PASS | docker login -u $USER --password-stdin 269815612133.dkr.ecr.us-east-1.amazonaws.com"
        sh "docker push ${IMAGE_NAME}${TAG}"
    }
}

def deployApp() {
    sh 'envsubst < kubernetes/deployment.yaml | kubectl apply -f -'
    sh 'envsubst < kubernetes/service.yaml | kubectl apply -f -'
}

return this
