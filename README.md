## Complete CI/CD Pipeline with EKS and AWS ECR

### Technologies used:

Kubernetes, Jenkins, AWS EKS, AWS ECR, Java, Maven, Linux, Docker, Git.

### Project Description:

1. Create a private AWS ECR Docker repository.

2. Adjust Jenkinsfile to build and push Docker image to AWS ECR.

3. Integrate deploying to K8s cluster in the CI/CD pipeline from AWS ECR private registry.

4. The complete CI/CD project built has the following configuration:

    * CI step: Increment version

    * CI step: Build artifact for the Java Maven application

    * CI step: Build and push Docker image to AWS ECR

    * CD step: Deploy new application version to EKS cluster 

    * CD step: Commit the version update

### Instructions:

#### Step 1: Install and configure Jenkins and Docker engine on an AWS EC2 Server:

![image](images/jenkins-1.png)

#### Step 2: Create AWS EKS cluster with a node group of 3 nodes using AWS console or `eksctl` CLI tool or Terraform:

![image](images/eks-1.png)

![image](images/eks-2.png)

##### Upload the `kubeconfig` file for the cluster into the `.kube` folder in the home directory of the 'jenkins' user on the Jenkins server: 

![image](images/jenkins-2.png)

##### Create credentials (secret text) in Jenkins for AWS Access Key ID and Secret Access Key to access AWS EKS cluster from Jenkins: 

![image](images/jenkins-credential-1.png)

![image](images/jenkins-credential-2.png)

#### Step 3: Create an AWS ECR private repository to store Docker image versions for the Java Maven application:

![image](images/ecr-1.png)

##### Create `ECRLogin` credential (username with password) in Jenkins to enable login and pushing to the private repository on ECR:
  
![image](images/jenkins-credential-3.png)

##### Create a `secret` K8s component to faciliate pulling a Docker image from the ECR repository and deploying it within the EKS cluster:
  
```
# secret.yaml K8 manifest file

apiVersion: v1
kind: Secret
metadata:
  name: docker-registry-key
data:
  .dockerconfigjson: <`base64 ~/.docker/config.json -w 0`>
type: kubernetes.io/dockerconfigjson
```

```
kubectl apply -f secret.yaml
```

![image](images/secret.png)

#### Step 4: Install `gettext-base` package for the `envsubst` tool on the Jenkins server to substitute environment variables in K8 manifest files within Jenkinsfile:

```
apt-get update
apt-get install gettext-base
```

#### Step 5: Write a Dockerfile to build a docker image for the Java Maven application:

```
FROM amazoncorretto:8-alpine3.17-jre

EXPOSE 8080

COPY ./target/java-maven-app-*.jar /usr/app/
WORKDIR /usr/app

CMD java -jar java-maven-app-*.jar
```

#### Step 6: Write K8 manifest files to create the `deployment` and `service` components for the Java application:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: $APP_NAME
  labels:
    app: $APP_NAME
spec:
  replicas: 2
  selector:
    matchLabels:
      app: $APP_NAME
  template:
    metadata:
      labels:
        app: $APP_NAME
    spec:
      imagePullSecrets:
        - name: docker-registry-key
      containers:
        - name: $APP_NAME
          image: "${IMAGE_NAME}${TAG}"
          imagePullPolicy: Always
          ports:
            - containerPort: 8080
```

```
apiVersion: v1
kind: Service
metadata:
  name: $APP_NAME
spec:
  selector:
    app: $APP_NAME
  ports:
    - protocol: TCP
      port: 80
      targetPort: 8080
  type: LoadBalancer
```

#### Step 7: Write a Groovy script to be called from within the Jenkinsfile:

```
def testApp() {
    sh 'mvn test'
}

def buildJar() {
    sh 'mvn clean package -Dmaven.test.skip=true'
}

def buildImageECR() {
    withCredentials([usernamePassword(credentialsId: 'ECRLogin', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh "docker build -t ${IMAGE_NAME}${TAG} ."
        sh "echo $PASS | docker login -u $USER --password-stdin 269815612133.dkr.ecr.us-east-1.amazonaws.com"
        sh "docker push ${IMAGE_NAME}${TAG}"
    }
}

def deployApp() {
    sh 'envsubst < kubernetes/deployment.yaml | kubectl apply -f -'
    sh 'envsubst < kubernetes/service.yaml | kubectl apply -f -'
}

return this
```

#### Step 8: Add all the required stages to the Jenkinsfile

```
#!/usr/bin/env groovy

def gv

pipeline {
    agent any
    tools {
        maven 'MAVEN3.9'
    }
    environment {
        REPO = '269815612133.dkr.ecr.us-east-1.amazonaws.com'
        APP_NAME = 'java-maven-app'

        IMAGE_NAME = "${REPO}/${APP_NAME}:"
    }
    stages {
        stage('Init') {
            steps {
                script {
                    gv = load 'script.groovy'
                }
            }
        }
        stage('Test') {
            steps { 
                script {
                    echo "Testing in branch ${BRANCH_NAME}.."
                    gv.testApp()
                }
            }
        }
        stage('Increment Version') {
            steps {
                script {
                    echo 'Incrementing App version..'
                    sh 'mvn build-helper:parse-version versions:set \
                    -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
                    versions:commit'
                    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
                    def version = matcher[0][1]

                    env.TAG = "$version-$BUILD_NUMBER"
                }
            }
        }
        stage('Build Jar') {
            steps {
                script {
                    echo 'Building the application Jar..'
                    gv.buildJar()
                }
            }
        }
        stage('Build Image') {
            steps {
                script {
                    echo "Building the Docker image: ${IMAGE_NAME}${TAG}.."
                    gv.buildImageECR()
                }
            }
        }
        stage('Deploy') {
            environment {
               AWS_ACCESS_KEY_ID = credentials('AWSAdminKeyID')
               AWS_SECRET_ACCESS_KEY = credentials('AWSSecretAccessKey')
            }
            steps {
                script {
                    echo 'Deploying the application to EKS..'
                    gv.deployApp()
                }
            }
        }
        stage('Commit Version Update')
        {
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: 'GitLabLogin', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                        sh 'git config --global user.email "jenkins@example.com"'
                        sh 'git config --global user.name "jenkins"'

                        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/devops-projects-skasap/complete-ci-cd-pipeline-with-eks-and-aws-ecr.git"
                        sh 'git add .'
                        sh 'git commit -m "CI: Version Bump"'
                        sh 'git push origin HEAD:main'
                    }
                }
            }
        }
    }
}
```
##### Jenkins CI/CD pipeline operation 

![image](images/jenkins-3.png)

##### Docker image versions in the private ECR repository

![image](images/ecr-2.png)

##### All components within the EKS cluster

![image](images/cluster.png)
