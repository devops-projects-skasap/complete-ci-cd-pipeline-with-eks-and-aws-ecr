#!/usr/bin/env groovy

def gv

pipeline {
    agent any
    tools {
        maven 'MAVEN3.9'
    }
    environment {
        REPO = '269815612133.dkr.ecr.us-east-1.amazonaws.com' // "docker.io/skasap" for DockerHub
        APP_NAME = 'java-maven-app'

        IMAGE_NAME = "${REPO}/${APP_NAME}:"
    }
    stages {
        stage('Init') {
            steps {
                script {
                    gv = load 'script.groovy'
                }
            }
        }
        stage('Test') {
            steps { 
                script {
                    echo "Testing in branch ${BRANCH_NAME}.."
                    gv.testApp()
                }
            }
        }
        stage('Increment Version') {
            steps {
                script {
                    echo 'Incrementing App version..'
                    sh 'mvn build-helper:parse-version versions:set \
                    -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
                    versions:commit'
                    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
                    def version = matcher[0][1]

                    env.TAG = "$version-$BUILD_NUMBER"
                }
            }
        }
        stage('Build Jar') {
            steps {
                script {
                    echo 'Building the application Jar..'
                    gv.buildJar()
                }
            }
        }
        stage('Build Image') {
            steps {
                script {
                    echo "Building the Docker image: ${IMAGE_NAME}${TAG}.."
                    // gv.buildImageDockerHub()
                    gv.buildImageECR()
                }
            }
        }
        stage('Deploy') {
            environment {
               AWS_ACCESS_KEY_ID = credentials('AWSAdminKeyID')
               AWS_SECRET_ACCESS_KEY = credentials('AWSSecretAccessKey')
            }
            steps {
                script {
                    echo 'Deploying the application to EKS..'
                    gv.deployApp()
                }
            }
        }
        stage('Commit Version Update')
        {
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: 'GitLabLogin', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                        sh 'git config --global user.email "jenkins@example.com"'
                        sh 'git config --global user.name "jenkins"'

                        // sh 'git status'
                        // sh 'git branch'
                        // sh 'git config --list'

                        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/devops-projects-skasap/complete-ci-cd-pipeline-with-eks-and-aws-ecr.git"
                        sh 'git add .'
                        sh 'git commit -m "CI: Version Bump"'
                        sh 'git push origin HEAD:main'
                    }
                }
            }
        }
    }
}